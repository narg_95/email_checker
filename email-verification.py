import socket
import smtplib
import dns.resolver
import re

def exist(email):
    match = re.match('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$', email)
    
    if match is None:
        return False

    return is_smtp_ok(email)
        
def is_smtp_ok(email):
	# Get local server hostname
	host = socket.gethostname()
	
	# SMTP lib setup (use debug level for full output)
	server = smtplib.SMTP()
	server.set_debuglevel(0)
	
	# SMTP Conversation
	server.connect(get_mx_record(email))
	server.helo(host)
	server.mail('me@domail.com')
	code, message = server.rcpt(str(email))
	server.quit()
	
	# Assume 250 as Success
	return code == 250
	
def get_mx_record(email):
    domain = email.split('@')[1]
    records = dns.resolver.query(domain, 'MX')
    mxRecord = records[0].exchange
    return str(mxRecord)
    
print (exist('narg_95@hotmail.com'))